﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using ASTMDemoSite.Classes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ASTMDemoSite.Classes;

namespace ASTMDemoSite.Classes.Tests
{
    [TestClass()]
    public class NumericOperationsTests
    {
        [TestMethod()]
        public void SumTest()
        {
            NumericOperations numOpr = new NumericOperations();
            Assert.AreEqual(5, numOpr.Sum(2, 3));
        }
    }
}