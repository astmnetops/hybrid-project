﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ASTMDemoSite.Classes;

namespace ASTMDemoSite
{
    public partial class _Default : Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btn_Add_Click(object sender, EventArgs e)
        {
            NumericOperations numOper = new NumericOperations();
            txt_Sum.Text = numOper.Sum(Convert.ToInt32(txt_IntOne.Text), Convert.ToInt32(txt_IntTwo.Text)).ToString();            
        }
    }
}