﻿<%@ Page Title="ASTM" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="ASTMDemoSite._Default" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">

    <div class="jumbotron">
        <h1>A Random Header</h1>
        <p class="lead">This is a Sample asp.Net based WebSite developed to demonstrate CI/CD Operations in ASTM</p>
        
        <p><a href="https://confluence.astm.org:8443/pages/viewpage.action?pageId=32637229" class="btn btn-primary btn-lg" target="_blank">Learn More &raquo;</a></p>
    </div>

    <div class="row">
        <div class="col-md-4">
            <h2>Bit Bucket</h2>
            <p>
                Bitbucket is a Version Control System from Atlassian, which uses Git Under the hood. Bitbucket is introduced for ASTM and is slowly adapted by projects.
            </p>
            <p>
                <a class="btn btn-default" href="https://bitbucket.astm.org:8443/projects" target="_blank">Learn more &raquo;</a>
            </p>
        </div>
        <div class="col-md-4">
            <h2>Azure DevOps</h2>
            <p>
                In the simplest terms, Azure DevOps is the evolution of VSTS (Visual Studio Team Services). 
            </p>
            <p>
                <a class="btn btn-default" href="https://dev.azure.com" target="_blank">Learn more &raquo;</a>
            </p>
        </div>
        <div class="col-md-4">
            <h2>Android!</h2>
            <p>
                Well, this space ought to be filled. So just know, Android Phones are the best! 
            </p>
            <p>
                <a class="btn btn-default" href="https://www.android.com/" target="_blank">Learn more &raquo;</a>
            </p>
        </div>

        <br />
        <asp:TextBox id="txt_IntOne" Text="1" runat="server" />
        <asp:TextBox id="txt_IntTwo" Text="2" runat="server" />
        <asp:Button id="btn_Add" Text="Get the Sum" runat="server" OnClick="btn_Add_Click" /> = 

        <asp:TextBox id="txt_Sum" Text="" runat="server" Enabled="false" />




    </div>

</asp:Content>
