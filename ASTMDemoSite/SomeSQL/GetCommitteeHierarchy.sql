﻿CREATE PROCEDURE [db_MEM].[GetCommitteeHierarchy]
(
@CommitteeId INT
)
AS
BEGIN
WITH CTE AS (
  SELECT COMMITTEE.CommitteeId,
         COMMITTEE.ParentCommitteeId        
    FROM [db_MEM].[Committee] AS COMMITTEE
   WHERE  COMMITTEE.CommitteeId=@CommitteeId AND  
    COMMITTEE.ParentCommitteeId IS NOT NULL
  UNION ALL
  SELECT 
		CHILD.CommitteeId,   
         CHILD.ParentCommitteeId   
    FROM [db_MEM].[Committee] AS CHILD
	JOIN CTE  ON CTE.ParentCommitteeId = CHILD.CommitteeId 	
	AND CHILD.ParentCommitteeId IS NOT NULL
  )

SELECT  CommitteeId,ParentCommitteeId  FROM CTE
END