﻿<%@ Page Title="Contact" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Contact.aspx.cs" Inherits="ASTMDemoSite.Contact" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <h2><%: Title %>.</h2>
    <h3>Contact Page</h3>
    <address>
        100 Barr Harbor Dr<br />
        Conshohocken, PA 19428<br />
        <abbr title="Phone">P:</abbr>
        425.555.0100
    </address>

    <address>        
        <strong>ASTM Production Support:</strong> <a href="mailto:ITProdSupport@astm.org">ITProdSupport@astm.org</a>
    </address>
</asp:Content>
